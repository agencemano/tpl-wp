<?php

/**
 * Helper functions.
 */
require get_stylesheet_directory().'/inc/extras.php';

function tpl_setup_theme ()
{
	load_child_theme_textdomain( 'sd-framework', get_stylesheet_directory() . '/languages' );
	remove_filter( 'excerpt_more', 'new_excerpt_more' );
}
add_action( 'after_setup_theme', 'tpl_setup_theme' );


if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( 'Réglages du thème' );
}

function tpl_post_types ()
{
	if (defined('WPSEO_PATH'))
	{
		$wpseo_front = WPSEO_Frontend::get_instance();
		remove_filter( 'pre_get_document_title', array( $wpseo_front, 'title' ), 15 );
		remove_filter( 'wp_title', array( $wpseo_front, 'title' ), 15 );
	}
	
	register_taxonomy( 'tpl_resource_type',
		array( 'tpl_resource' ),
		array(
			'labels'            => array(
				'name'          => 'Types',
				'singular_name' => 'Type',
				'search_items'  => 'Rechercher un type',
				'all_items'     => 'Tous les types',
				'edit_item'     => 'Modifier le type',
				'update_item'   => 'Mettre à jour le type',
				'add_new_item'  => 'Ajouter un nouveau type',
				'new_item_name' => 'Nom du nouveau type',
				'menu_name'     => 'Types',
			),
			'hierarchical'      => true,
			'show_admin_column' => true,
			'rewrite'           => array( 'slug'       => 'ressource/type',
			                              'with_front' => false,
			),
		)
	);
	
	register_post_type( 'tpl_resource',
		array(
			'labels'      => array(
				'name'               => 'Ressources',
				'singular_name'      => 'Ressource',
				'menu_name'          => 'Ressources',
				'name_admin_bar'     => 'Ressource',
				'add_new'            => 'Ajouter une nouvelle ressource',
				'add_new_item'       => 'Ajouter une nouvelle ressource',
				'new_item'           => 'Nouvelle ressource',
				'edit_item'          => 'Editer la ressource',
				'view_item'          => 'Voir la ressource',
				'all_items'          => 'Toutes les ressources',
				'search_items'       => 'Rechercher une ressource',
				'not_found'          => 'Aucune ressource trouvée.',
				'not_found_in_trash' => 'Aucune ressource dans la corbeille.',
			),
			'supports'    => array( 'title', 'thumbnail' ),
			'taxonomies'  => array( 'tpl_resource_type' ),
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => array( 'slug' => 'ressource' ),
			'menu_icon'   => 'dashicons-info',
		)
	);
}
add_action( 'init', 'tpl_post_types' );


function tpl_wp_title( $title ) {
	if ( is_tax() ) {
		$term = get_queried_object();
		if ( $term ) {
			$taxonomy_parent = get_term_top_most_parent($term, 'tpl_resource_type');
			$title = $taxonomy_parent->name;
		}
	} else if ( function_exists( 'get_field' ) ) {
		$t = '';
		if ( is_category( get_field( 'pro_cat', 'option' ) ) ) {
			$t = get_field( 'pro_archive_title', 'option' );
		} else if ( is_category( get_field( 'research_cat', 'option' ) ) ) {
			$t = get_field( 'research_archive_title', 'option' );
		}
		if ( ! empty( $t ) ) {
			$title = $t;
		}
	}
	
	return $title;
}

add_filter( 'wp_title', 'tpl_wp_title', 99);

function tpl_pre_get_posts( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}
	
	/*if (is_home()) {
		// Display only 1 post for the original blog archive
		$query->set( 'posts_per_page', 1 );
		return;
	}*/
	
	if ( is_tax() ) {
		$term = get_queried_object();
		if ( $term && function_exists( 'get_field' ) ) {
			$link_type   = get_field( 'link_type', 'option' );
			$book_type   = get_field( 'book_type', 'option' );
			$member_type = get_field( 'member_type', 'option' );
			switch ( $term->term_id ) {
				case $link_type:
					$ppp = 15;
					break;
				
				case $book_type:
					$ppp = 8;
					break;
				
				case $member_type:
					$ppp = 12;
					break;
				
				default:
					$ppp = 4;
					break;
			}
			$query->set( 'posts_per_page', $ppp );
			
			return;
		}
	}
}

add_action( 'pre_get_posts', 'tpl_pre_get_posts', 1 );

function tpl_print_scripts ()
{
	wp_dequeue_script( 'mobile-menu' );
}

add_action( 'wp_print_scripts', 'tpl_print_scripts' );

function tpl_scripts ()
{
	wp_register_script( 'mobile-menu-tpl', get_stylesheet_directory_uri() . '/framework/js/mobile-menu.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'mobile-menu-tpl' );
}

add_action( 'wp_enqueue_scripts', 'tpl_scripts' );

function tpl_excerpt_more ($more)
{
	global $post;
	return '...<br><a class="moretag" href="'. get_permalink($post->ID) . '">Lire la suite</a>';
}
add_filter('excerpt_more', 'tpl_excerpt_more');







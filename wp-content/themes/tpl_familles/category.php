<?php
/* ------------------------------------------------------------------------ */
/* Theme Archive
/* ------------------------------------------------------------------------ */
get_header();
$i = 1;

global $sd_page_template;
$sd_page_template = true;

global $tpl_news_full_width;
$tpl_news_full_width = false;

$blog_sidebar     = ( ! empty( $sd_data['blog_sidebar'] ) ? $sd_data['blog_sidebar'] : '' );
$theme_pagination = ( ! empty( $sd_data['theme_pagination'] ) ? $sd_data['theme_pagination'] : '' );
?>
<div class="container content">
	<div class="row">
		<?php if ( have_posts() ) : ?>
			<div class="span2"></div>
			<div class="span8">
				<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php if ( $i == 1 ) : ?>
							<div>
								<?php get_template_part( 'framework/inc/post-formats/first-post/content'); ?>
							</div>
						<?php else : ?>
							<?php get_template_part( 'framework/inc/post-formats/content'); ?>
						<?php endif; ?>
						<?php $i ++; ?>
					<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
<!--pagination-->
<?php sd_custom_pagination(); ?>
<!--pagination end-->
<?php get_footer(); ?>

<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Page: Contact
/* ------------------------------------------------------------------------ */
get_header();

if ( have_posts() ) : while ( have_posts() ) :
	the_post();
	?>
	<?php if ( function_exists( 'get_field' ) ) : ?>
	<div class="container sd-center sd-margin-bottom">
		<?php echo apply_filters('the_content', get_field( 'contact_intro' )); ?>
	</div>
<?php endif; ?>
	<div class="container">
		<div class="grid-2">
			<div class="contact-column">
				<?php
				if ( function_exists( 'get_field' ) ) : ?>
					<h3 class="sd-styled-title"><?php the_field('form_title'); ?></h3>
					<?php
					echo do_shortcode( get_field( 'form_shortcode' ) );
				endif;
				?>
			</div>
			<div>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>

<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Page: A propos de nous
/* ------------------------------------------------------------------------ */
get_header();

if ( have_posts() ) : while ( have_posts() ) :
	the_post();

global $sd_data;

// Members query args
$members_query_args = array(
	'post_type'      => 'tpl_resource',
	'posts_per_page' => - 1,
);

// ACF fields
if ( function_exists( 'get_field' ) ) {
	$featured_members_title = get_field( 'featured_members_title' );
	$featured_members       = get_field( 'featured_members' );
	$other_members_title    = get_field( 'other_members_title' );
	$other_members          = get_field( 'other_members' );
}

?>
	<div class="container">
			<article
				id="post-<?php the_ID(); ?>" <?php post_class( 'blog-entry page-entry clearfix' ); ?>>
				<!-- entry content -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<!-- entry content end-->
			</article>
			<!--post-end-->
		<?php endwhile; endif; ?>
	</div>
	
	<div class="members-lists">
		<?php if ( $featured_members && count( $featured_members ) > 0 ) : $i = 1; ?>
			<div class="container">
				<?php if ( $featured_members_title != '' ) : ?>
					<h3 class="sd-styled-title"><?php echo $featured_members_title; ?></h3>
					<div class="space-divider"
					     style="padding-bottom: 20px; margin-top: 0;"></div>
				<?php endif; ?>
				<?php foreach ( $featured_members as $fm ) :
					$name = $fm->post_title;
					$subtitle = ( function_exists( 'get_field' ) ) ? get_field( 'resource_desc', $fm->ID ) : '';
					$image = ( has_post_thumbnail( $fm->ID ) ) ? '<img src="' . get_the_post_thumbnail_url( $fm->ID ) . '" alt="' . $name . '" title="' . $name . '" />' : '';
					$link = ( function_exists( 'get_field' ) ) ? get_field( 'resource_url', $fm->ID ) : '';
					?>
					<div class="one-fourth <?php if ( $i % 4 == 0 ) {
						echo 'last';
					} ?>">
						<div class="sd-organizer tpl-resource tpl-member">
							<?php if (!empty($link)) : ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>
							<?php echo $image; ?>
							<?php if (!empty($link)) : ?></a><?php endif; ?>
							<div class="sd-organizer-details">
								<?php if (!empty($link)) : ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>
								<h3><?php echo $name; ?></h3>
									<?php if (!empty($link)) : ?></a><?php endif; ?>
								<p><?php echo apply_filters('the_content', $subtitle); ?></p>
							</div>
						</div>
					</div>
					<?php
					if ( $i % 4 == 0 ) {
						echo '<div class="clearfix"></div>';
					}
					$i ++;
					?>
				<?php endforeach; ?>
				<div class="clearfix"></div>
			</div>
		<?php endif; ?>
		
		<?php if ( $other_members && count( $other_members ) > 0 ) : ?>
			<div class="container">
				<?php if ( $other_members_title != '' ) : ?>
					<h3 class="sd-styled-title"><?php echo $other_members_title; ?></h3>
					<div class="space-divider"
					     style="padding-bottom: 20px; margin-top: 0;"></div>
				<?php endif; ?>
				<ul>
					<?php foreach ( $other_members as $om ) :
						$name = $om->post_title;
						$subtitle = ( function_exists( 'get_field' ) ) ? get_field( 'resource_desc', $om->ID ) : '';
						$link = ( function_exists( 'get_field' ) ) ? get_field( 'resource_url', $om->ID ) : '';
						?>
						<li class="tpl-member-other">
							<h5>
								<?php if (!empty($link)) : ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>
								<?php echo $name; ?>
								<?php if (!empty($link)) : ?></a><?php endif; ?>
							</h5> 
							<?php if (!empty($subtitle)) : ?>
								<div><?php echo apply_filters('the_content', $subtitle); ?></div>
							<?php endif; ?>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>
<?php
/* ------------------------------------------------------------------------ */
/* Theme Single Post
/* ------------------------------------------------------------------------ */
get_header(); ?>
<!--left col-->

<div class="container content">
<div class="row">
<!--left col-->
<div class="span2"></div>
<div class="span8">
	<?php if (have_posts()) : while (have_posts()) : the_post();?>
	<?php get_template_part( 'framework/inc/post-formats/single', get_post_format() ); ?>
	<?php endwhile; endif; ?>
	<!--comments-->
	<?php comments_template('', true); ?>
	<!--comments end--> 
</div>
<!--left col end-->
</div>
</div>
<!--sidebar end-->
<?php get_footer(); ?>
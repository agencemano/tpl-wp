jQuery(document).ready(function($) {
	
	function updateTab () {
		var hash = $(location).attr('hash');
		if (hash !== '') {
			var $target =  $("a[href='"+hash+"']");
			$target.click();
		}
	}
	
	$(window).on('hashchange', function(e) {
		e.preventDefault();
		updateTab();
	});
	
	setTimeout(function () {
		$("a.ui-tabs-anchor").click(function () {
			var link = $(this).attr('href');
			history.pushState({}, null, link);
		});
		
		updateTab();
	}, 500);
	
});

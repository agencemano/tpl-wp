<?php
$desc = (function_exists('get_field')) ? get_field('resource_desc') : '';
$image = ( has_post_thumbnail() ) ? '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'medium') . '" alt="' . get_the_title() . '" title="' . get_the_title() . '" />' : '';
$link = (function_exists('get_field')) ? get_field('resource_url') : '';
?>
<div class="tpl-resource tpl-link">
	<div class="image">
		<?php if (!empty($link)) : ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>
			<?php echo $image; ?>
			<?php if (!empty($link)) : ?></a><?php endif; ?>
	</div>
	<div class="details">
		<?php if (!empty($link)) : ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>
			<h3><?php the_title(); ?></h3>
			<?php if (!empty($link)) : ?></a><?php endif; ?>
		<p><?php echo apply_filters('the_content', $desc); ?></p>
	</div>
</div>

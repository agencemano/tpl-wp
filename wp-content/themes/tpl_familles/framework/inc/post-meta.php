<?php
/* ------------------------------------------------------------------------ */
/* Post Meta
/* ------------------------------------------------------------------------ */
?>
<!-- entry meta -->

<aside class="entry-meta clearfix">
	<ul>
		<li class="meta-date">
			<?php _e('On: ', 'sd-framework'); ?><span class="meta-gray"><?php the_time(get_option('date_format')); ?></span>
		</li>
	</ul>
</aside>
<!-- entry meta end --> 
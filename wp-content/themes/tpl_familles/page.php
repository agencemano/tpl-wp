<?php 
/* ------------------------------------------------------------------------ */
/* Theme Normal Page
/* ------------------------------------------------------------------------ */
get_header(); 
global $sd_data;
?>
<!--left col-->

<div class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post();?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry page-entry clearfix'); ?>> 
		
		<!-- entry content -->
		<div class="entry-content"> 
				<?php the_content(); ?>
				<?php wp_link_pages('before=<strong class="page-navigation clearfix">&after=</strong>'); ?>
		</div>
		<!-- entry content end--> 
	</article>
	<!--post-end-->
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>
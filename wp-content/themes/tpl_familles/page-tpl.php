<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Page: TPL
/* ------------------------------------------------------------------------ */
get_header();

if ( have_posts() ) : while ( have_posts() ) :
	the_post();

// ACF fields
if ( function_exists( 'get_field' ) )
{
	$tabs = get_field( 'tpl_tabs' );
	if ($tabs && count($tabs) > 0)
	{
		ob_start();
		$menu = '<ul class="clearfix sd-tab-titles">';
		foreach ($tabs as $tab)
		{
			$slug = sanitize_title($tab['tab_title']);
			$menu .= "<li><a href='#{$slug}'>{$tab['tab_title']}</a></li>";
			?>
			<div id="<?php echo $slug; ?>" class="sd-tab">
				<?php echo apply_filters('the_content', $tab['tab_content']); ?>
			</div>
			<?php
		}
		$menu .= '</ul>';
		$content = ob_get_clean();
	}
}

endwhile;
endif;

?>
<?php if (isset($menu) && isset($content)) : ?>
	<div class="container">
		<div id="sd-tabs-tpl" class="sd-tabs sd-tabs-visibility no-js sd-vertical-tabs ">
			<div class="sd-tab-content">
				<?php echo $menu; ?>
				<?php echo $content; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>
<?php
/* ------------------------------------------------------------------------ */
/* Theme Archive
/* ------------------------------------------------------------------------ */
get_header();

$taxonomy_parent = get_term_top_most_parent(get_queried_object(), 'tpl_resource_type');
?>

<div class="container">
	<?php get_template_part('template-parts/taxonomy/content', $taxonomy_parent->slug); ?>
</div>

<!--pagination-->
<?php sd_custom_pagination(); ?>

<!--pagination end-->
<?php get_footer(); ?>

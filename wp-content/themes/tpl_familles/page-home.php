<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Page: Home
/* ------------------------------------------------------------------------ */
get_header();

if ( have_posts() ) : while ( have_posts() ) :
the_post();

// Header background
$header_image = get_the_post_thumbnail_url( get_the_ID(), 'full' );

// News query args
$news_query_args = array(
	'post_type' => 'post',
	'posts_per_page' => 3,
);

// ACF fields
if ( function_exists( 'get_field' ) )
{
	// Donation fields
	$donation_title     = get_field( 'donation_title' );
	$donation_text      = get_field( 'donation_text' );
	$donation_form_url  = get_field( 'donation_form_url' );
	$donation_link_text = get_field( 'donation_link_text' );
	
	// Push blocks
	$push_columns = get_field( 'push_columns' );
	if ( $push_columns ) {
		switch ( count( $push_columns ) ) {
			case 4:
				$push_class = 'span3';
				break;
			case 3:
				$push_class = 'span4';
				break;
			case 2:
				$push_class = 'span6';
				break;
			default:
				$push_class = '';
				break;
		}
	}
	
	// News query
	$featured_posts = get_field('featured_posts');
	if ($featured_posts && !empty($featured_posts))
	{
		$news_query_args['post__in'] = $featured_posts;
		$news_query_args['orderby'] = 'post__in';
	}
	else
	{
		$news_cat = get_field('news_cat', 'option');
		if ($news_cat)
			$news_query_args['cat'] = $news_cat;
	}
}

// News query
$news_query = new WP_Query($news_query_args);
$i = 1;

global $sd_page_template;
global $tpl_news_full_width;
$tpl_news_full_width = false;
?>

<div class="home-intro"
     style="background-image: url('<?php echo $header_image; ?>');">
	<div class="container">
		<?php if ( $donation_title ): ?>
			<h1><?php echo $donation_title; ?></h1><?php endif; ?>
		<?php if ( $donation_text ): ?>
			<p class="donation-text"><?php echo $donation_text; ?></p><?php endif; ?>
		<?php if ( $donation_form_url && $donation_link_text ) : ?>
			<a href="<?php echo $donation_form_url; ?>" target="_blank"
			   class="accent-background sd-opacity-trans tpl-donate-button"><?php echo $donation_link_text; ?></a>
		<?php endif; ?>
	</div>
	<?php if ( $push_columns && count( $push_columns ) > 0 ) : ?>
		<div class="accent-background container home-push-blocks">
			<div class="row">
				<?php foreach ( $push_columns as $push_block ) : ?>
					<div class="<?php echo $push_class; ?> push-block">
						<a href="<?php echo $push_block['page_link']['url']; ?>" target="<?php echo $push_block['page_link']['target']; ?>">
							<div class="title">
								<i class="fa fa-<?php echo $push_block['block_icon']; ?>"
								   aria-hidden="true"></i>
								<span><?php echo $push_block['page_link']['title']; ?></span>
							</div>
							<div class="content"><?php echo apply_filters( 'the_content', $push_block['block_text'] ); ?></div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php if ( $news_query->have_posts() ) : ?>
<div class="container content">
	<div class="row">
		<?php if ( $tpl_news_full_width ) : ?>
		<div class="span12">
			<?php else : ?>
			<div class="span2"></div>
			<div class="span8">
				<?php endif; ?>
				<?php
				while ( $news_query->have_posts() ) : $news_query->the_post();
					if ( $i == 1 ) :
						?>
						<div>
							<?php get_template_part( 'framework/inc/post-formats/first-post/content', get_post_format() ); ?>
						</div>
					<?php else : ?>
						<?php get_template_part( 'framework/inc/post-formats/content', get_post_format() ); ?>
					<?php
					endif;
					$i ++;
				endwhile;
				?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<!-- content end -->
	<?php
	endwhile;
	endif;
	
	get_footer();
	?>
	
	
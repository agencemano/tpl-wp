<?php
/* ------------------------------------------------------------------------ */
/* Template name: Page: Blog News Style
/* ------------------------------------------------------------------------ */
get_header();

global $sd_data;
global $sd_page_template;
$sd_page_template = true;

global $tpl_news_full_width;
$tpl_news_full_width = false;

$blog_sidebar     = ( ! empty( $sd_data['blog_sidebar'] ) ? $sd_data['blog_sidebar'] : '' );
$theme_pagination = ( ! empty( $sd_data['theme_pagination'] ) ? $sd_data['theme_pagination'] : '' );
?>

<div class="container content">
	<div class="row">
		<!--left col-->
		<?php if ( $tpl_news_full_width ) : ?>
		<div class="span12">
			<?php else : ?>
			<div class="span2"></div>
			<div class="span8">
				<?php endif; ?>
				<div class="row">
					<?php
					global $wp_query;
					global $more;
					$col   = 1;
					$i     = 1;
					$more  = 0;
					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					$args  = array(
						'post_type'   => 'post',
						'post_status' => 'publish',
						'orderby'     => 'date',
						'order'       => 'DESC',
						'paged'       => $paged,
					);
					
					if ( function_exists( 'get_field' ) ) {
						// News cat
						$news_cat = get_field( 'news_cat', 'option' );
						if ( $news_cat ) {
							$args['cat'] = $news_cat;
						}
					}
					
					$wp_query = new WP_Query( $args );
					
					if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
						?>
						<?php if ( $i == 1 ) :
							?>
							<div>
								<?php get_template_part( 'framework/inc/post-formats/first-post/content', get_post_format() ); ?>
							</div>
						<?php else : ?>
							<?php get_template_part( 'framework/inc/post-formats/content', get_post_format() ); ?>
						<?php
						endif; ?>
						<?php $i ++; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					<!--pagination-->
					<?php if ( $sd_data['theme_pagination'] == 1 ) : ?>
						<?php if ( get_previous_posts_link() ) : ?>
							<div class="nav-previous">
								<?php previous_posts_link( __( 'Previous Posts', 'sd-framework' ) ); ?>
							</div>
						<?php endif; ?>
						<?php if ( get_next_posts_link() ) : ?>
							<div class="nav-next">
								<?php next_posts_link( __( 'Next Posts', 'sd-framework' ) ); ?>
							</div>
						<?php endif; ?>
					<?php elseif ( $sd_data['theme_pagination'] == 2 ) : ?>
						<?php sd_custom_pagination(); ?>
					<?php endif; ?>
					<!--pagination end-->
				</div>
			</div>
			<!--left col end-->
		</div>
	</div>
	<?php get_footer(); ?>
